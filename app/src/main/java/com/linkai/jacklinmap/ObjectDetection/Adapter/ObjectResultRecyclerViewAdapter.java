package com.linkai.jacklinmap.ObjectDetection.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.linkai.jacklinmap.ObjectDetection.tflite.Classifier;
import com.linkai.jacklinmap.R;
import com.linkai.jacklinmap.Utils.COCOCategory;
import com.linkai.jacklinmap.Utils.MyCategory;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * 创建日期：2021/6/22 15:46
 * @author 林凯
 * 文件名称： ObjectResultRecyclerViewAdapter.java
 * 类说明： 展示识别结果的 RecycleView 用到的 Adapter
 */
public class ObjectResultRecyclerViewAdapter extends RecyclerView.Adapter<ObjectResultRecyclerViewAdapter.ViewHolder> {

    private LayoutInflater mInflater;

    List<Classifier.Recognition> results;


    COCOCategory coocCategory;
    MyCategory myCategory;

    String modelType;

    public ObjectResultRecyclerViewAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void setResult(List<Classifier.Recognition> results) {
        this.results = results;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    /**
     * item显示类型
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.od_recyclerview_item_object_result, parent, false);
        //view.setBackgroundColor(Color.RED);

        coocCategory = new COCOCategory();
        myCategory = new MyCategory();

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }


    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        if (modelType.equals("yolov5s-fp16.tflite")) {
            ArrayList<String> category = coocCategory.getCategory();
            String type = category.get(results.get(position).getDetectedClass());
            holder.tv_type.setText(type);
        } else if (modelType.equals("yolov5s_ae86_640-fp16.tflite")) {
            ArrayList<String> category = myCategory.getCategory();
            String type = category.get(results.get(position).getDetectedClass());
            holder.tv_type.setText(type);
        }


        int left = Math.round(results.get(position).getLocation().left);
        int top = Math.round(results.get(position).getLocation().top);
        int right = Math.round(results.get(position).getLocation().right);
        int bottom = Math.round(results.get(position).getLocation().bottom);

        holder.tv_position.setText("[" + left + "," + top + "," + right + "," + bottom + "]");

        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        String score = decimalFormat.format(results.get(position).getConfidence());
        holder.tv_score.setText(score);


        // 别忘记设置 tag，整个 item 要设置，里面的控件如果有点击事件，也要设置
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }


    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_type, tv_position, tv_score;

        public ViewHolder(View view) {
            super(view);
            tv_type =  view.findViewById(R.id.object_recyclerview_type);
            tv_position = view.findViewById(R.id.object_recyclerview_position);
            tv_score = view.findViewById(R.id.object_recyclerview_score);
        }
    }
}