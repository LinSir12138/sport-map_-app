package com.linkai.jacklinmap.Fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.linkai.jacklinmap.Adapter.MyFragmentPagerAdapter;
import com.linkai.jacklinmap.R;

/**
 * 创建日期：2021/6/10 10:04
 * @author 林凯
 * 文件名称： HomeBottomFragment.java
 * 类说明： Home 界面底部的导航条对应的 Fragment
 */
public class HomeBottomFragment extends Fragment implements RadioGroup.OnCheckedChangeListener,
        ViewPager.OnPageChangeListener{


    RadioButton btn_home_run, btn_home_community, btn_home_mine;
    RadioGroup rg_tab_bar;
    ViewPager vpager;
    MyFragmentPagerAdapter mAdapter;

    //几个代表页面的常量
    public static final int PAGE_ONE = 0;
    public static final int PAGE_TWO = 1;
    public static final int PAGE_THREE = 2;

    public HomeBottomFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // 指定 fragment 需要填充的内容是什么
    /*
        指定 fragment 需要填充的内容是什么
        填充的内容：  R.layout.fragment_home_bottom
        填充到哪里去： container （就是他的一个父容器）
        是否绝对显示在他的父容器里面： false
    * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_bottom, container, false);
    }


    // 在 Fragment 与 Activity 通信时，必须要等到所有的Fragment都已经初始化完成之后，才能够开始进行，所以要写在 onActivityCreated 里面
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();
    }

    private void init() {

        // 实例化 Adapter
        mAdapter = new MyFragmentPagerAdapter(this.getActivity().getSupportFragmentManager(), 1);


        // 实例化 ViewPage,同时设置适配器
        vpager = (ViewPager) this.getActivity().findViewById(R.id.vpager);
        vpager.setAdapter(mAdapter);
        vpager.setCurrentItem(0);
        vpager.addOnPageChangeListener(this);

        // 底部按钮实例化并添加监听事件
        rg_tab_bar = (RadioGroup) this.getActivity().findViewById(R.id.rg_tab_bar);
        btn_home_community = this.getActivity().findViewById(R.id.rb_community);
        btn_home_run = this.getActivity().findViewById(R.id.rb_run);
        btn_home_mine = this.getActivity().findViewById(R.id.rb_mine);
        // 单选按钮组添加监听事件
        rg_tab_bar.setOnCheckedChangeListener(this);
        btn_home_community.setChecked(true);
    }


    // 底部单选按钮发生改变
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            // 首页
            case R.id.rb_community:
                vpager.setCurrentItem(PAGE_ONE);
                System.out.println(vpager);
                System.out.println("COmm");
                break;

            // 跑步
            case R.id.rb_run:
                vpager.setCurrentItem(PAGE_TWO);
                System.out.println(vpager);
                System.out.println("Run");
                break;

            // 我的
            case R.id.rb_mine:
                vpager.setCurrentItem(PAGE_THREE);
                System.out.println(vpager);
                System.out.println("Mine");
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        System.out.println("Scroll!!");
    }

    @Override
    public void onPageSelected(int position) {
        System.out.println("Select!!");
    }

    @Override
    public void onPageScrollStateChanged(int state) {
//state的状态有三个，0表示什么都没做，1正在滑动，2滑动完毕
        if (state == 2) {
            switch (vpager.getCurrentItem()) {
                case PAGE_ONE:
                    btn_home_community.setChecked(true);
                    break;
                case PAGE_TWO:
                    btn_home_run.setChecked(true);
                    break;
                case PAGE_THREE:
                    btn_home_mine.setChecked(true);
                    break;
            }
        }
    }
}