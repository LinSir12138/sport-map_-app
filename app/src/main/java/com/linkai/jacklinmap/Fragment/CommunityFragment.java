package com.linkai.jacklinmap.Fragment;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.linkai.jacklinmap.Database.MyHelper;
import com.linkai.jacklinmap.Activity.Community.DynamicDetailActivity;
import com.linkai.jacklinmap.R;
import com.linkai.jacklinmap.Activity.Community.ShareActivity;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * 创建日期：2021/6/12 11:27
 * @author 林凯
 * 文件名称： CommunityFragment.java
 * 类说明： 首页展示动态的 Fragment
 */
public class CommunityFragment extends Fragment implements View.OnClickListener {


    FloatingActionButton float_btn;
    ListView listView;
    ImageView tempImageView;        // 用来存放图片的 ImageView

    TextView community_bar_follow, community_bar_video;
    ImageView community_bar_search;

    // 数据库操作
    SQLiteDatabase db;
    MyHelper dbHelper;
    ContentValues values;

    // 先是在 initData() 中从数据读取数据存放到这里，然后在 Adapter 中实现数据绑定
    // 整个 ListView 的信息存在 dataList 中，其中的每一个 item 就是 mapItem
    ArrayList<HashMap<String, String>> allDynamics;
    // 动态数据的每一项
    HashMap<String, String> dynamicItem;


    // 全部动态的 Image 存放在 map 中，key是动态的id，value 就是 bitmapArrayList 对象
    HashMap<String, ArrayList<Bitmap>> imagesBitmapMap;
    // 存放图片对应 bitmap 的List（从数据库中读取之后，转换成bitmap再存入）, 每一个 bitmapArrayList 对象就是一条动态的所有图片
    ArrayList<Bitmap> bitmapArrayList;

    // 每设置完一条动态的图片是，需要重新赋值为 0
    int imagesCount = 0;        // 记录当前总共添加了多少张图片（方便添加图片到布局中，最多添加9张图片）


    public CommunityFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_community, container, false);
    }


    // 在 Fragment 与 Activity 通信时，必须要等到所有的Fragment都已经初始化完成之后，才能够开始进行，所以要写在 onActivityCreated 里面
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // 实例化数据库工具类
        dbHelper = new MyHelper(this.getContext());

        // 初始化数据(先初始化数据)
        initData();

        // 初始化控件
        init();
    }

    private void initData() {

        // 从数据库中读取动态信息并展示
        db = dbHelper.getReadableDatabase();
        imagesBitmapMap = new HashMap<>();

        allDynamics = new ArrayList<HashMap<String, String>>();

        Cursor cursor = db.rawQuery("select * from dynamic order by d_id desc", null);
        if (cursor.getCount() == 0) {
//            Toast.makeText(this.getContext(), "没有动态数据", Toast.LENGTH_SHORT).show();
        } else {
            cursor.moveToFirst();
            do {
                dynamicItem = new HashMap<>();

                //                    0     1        2        3           4         5
                // 查询到的字段依次为 d_id   u_id    title   content     privacy     c_time
                dynamicItem.put("did", cursor.getString(0));
                dynamicItem.put("uid", cursor.getString(1));
                dynamicItem.put("title", cursor.getString(2));
                dynamicItem.put("content", cursor.getString(3));
                dynamicItem.put("privacy", cursor.getString(4));
                dynamicItem.put("c_time", cursor.getString(5));

                // 根据id查询用户名
                Cursor cursorUname = db.rawQuery("select u_name from userinfo where u_id = ?", new String[]{cursor.getString(1)});
                //存在数据才返回true
                if(cursorUname.moveToFirst()) {
                    dynamicItem.put("uname", cursorUname.getString(0));
                } else {
                    System.out.printf("没有查到uname");
                    System.out.println("u_id = " + cursor.getString(1));
                    System.out.println(dynamicItem);
                }
                cursorUname.close();

                allDynamics.add(dynamicItem);
            } while (cursor.moveToNext());

        }

        Cursor cursorImage = db.rawQuery("select * from images", null);

        try {
            Field field = CursorWindow.class.getDeclaredField("sCursorWindowSize");
            field.setAccessible(true);
            field.set(null, 100 * 1024 * 1024); //the 100MB is the new size
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (cursorImage.getCount() == 0) {
//            Toast.makeText(this.getContext(), "没有图片", Toast.LENGTH_SHORT).show();
        } else {
            cursorImage.moveToFirst();
            cursorImage.getCount();
            do {
                //   0         1                         2                                       3
                //  i_id      d_id                       u_id                                bitmap
                // 图片id    动态id（如果是头像置为-1） 用户头像id（如是动态中的图片，则置为-1）图片对应的2进制


                bitmapArrayList = new ArrayList<>();


                // 首先获得这条记录的 bitmap 和 d_id
                Bitmap bmpout = BitmapFactory.decodeByteArray(cursorImage.getBlob(3), 0, cursorImage.getBlob(3).length);
                String d_id = cursorImage.getString(1);

                // 判断该 动态id是否已经添加到 map 中了
                if (imagesBitmapMap.get(d_id) != null) {
                    // 如果有，先取出该 key 对应的 list来，添加 bitmap 到list中，然后在存入 map
                    ArrayList<Bitmap> tempBitMapList = imagesBitmapMap.get(d_id);
                    tempBitMapList.add(bmpout);
                    imagesBitmapMap.put(d_id, tempBitMapList);
                } else {
                    // 如果没有，则创建一个 List，将 bitmap 加到 list中，然后将list加到map中
                    ArrayList<Bitmap> bitmaps = new ArrayList<>();
                    bitmaps.add(bmpout);
                    imagesBitmapMap.put(d_id, bitmaps);
                }


            } while (cursorImage.moveToNext());
        }


        // 关闭连接
        cursor.close();
        cursorImage.close();
        db.close();
    }

    private void init() {
        listView = this.getActivity().findViewById(R.id.community_listview);
        float_btn = this.getActivity().findViewById(R.id.community_float_btn);
        community_bar_follow = this.getActivity().findViewById(R.id.community_bar_follow);
        community_bar_video = this.getActivity().findViewById(R.id.community_bar_video);
        community_bar_search = this.getActivity().findViewById(R.id.community_bar_search);


        // 创建自定义Adapter，继承BaseAdapter 因为 baseAdapter 是一个抽象类
        CustomBaseAdapter customBaseAdapter = new CustomBaseAdapter();
        listView.setAdapter(customBaseAdapter);


        // 设置 ListView 的 item 项的点击事件
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                /*
                    如果将图片直接通过 Intent 传递给 DynamicDetailActivity，图片太大，会报错
                    所以这里只传递动态的数据，然后在 DynamicDetailActivity 中查询数据库获得具体数据
                * */


                    // 传递 map 给另一个 Activity。
                    HashMap<String, String> dataMap = allDynamics.get(i);
                    ArrayList<HashMap<String, String>> tempList = new ArrayList<>();
                    System.out.println("dataMap");
                    System.out.println(dataMap);
                    tempList.add(dataMap);


                    Intent intent = new Intent();
                    intent.setClass(getContext(), DynamicDetailActivity.class);
                    Bundle bundle = new Bundle();

                    //须定义一个list用于在budnle中传递需要传递的ArrayList<Object>,这个是必须要的
                    ArrayList bundlelist = new ArrayList();
                    bundlelist.add(tempList);

                    bundle.putParcelableArrayList("data",bundlelist);

                    intent.putExtras(bundle);
                    startActivity(intent);
            }
        });

        // 浮动按钮，转发，评论，点赞按钮添加点击事件
        float_btn.setOnClickListener(this::onClick);
        community_bar_follow.setOnClickListener(this::onClick);
        community_bar_video.setOnClickListener(this::onClick);
        community_bar_search.setOnClickListener(this::onClick);

    }


    // 定义一个内部类，继承自 BaseAdapter，并重写其方法
    private class CustomBaseAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return allDynamics.size();
        }

        @Override
        public Object getItem(int i) {
            return allDynamics.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        // 这个方法事件我们 ListView 里面为 item 定义的样式文件转换成为一个 View 对象
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {


            // 优化的 BaseAdapter，使用 ViewHolder 不适用 LayoutInflater，上面的要注释掉
            ViewHolder holder;
            if (view == null) {
                view = View.inflate(CommunityFragment.this.getActivity(), R.layout.listview_item_dynamic, null);
                holder = new ViewHolder();

                // 实例化 list_item 中的 TextView 控件
                holder.tv_uname = (TextView) view.findViewById(R.id.dynamic_item_uname);
                holder.tv_time = (TextView) view.findViewById(R.id.dynamic_item_time);
                holder.tv_title = (TextView) view.findViewById(R.id.dynamic_item_title);
                holder.tv_content = (TextView) view.findViewById(R.id.dynamic_item_content);
                holder.layout_share = view.findViewById(R.id.dynamic_item_layout_share);
                holder.layout_comment = view.findViewById(R.id.dynamic_item_layout_comment);
                holder.layout_star = view.findViewById(R.id.dynamic_item_layout_star);

                holder.layout_01 = view.findViewById(R.id.community_layout_01);
                holder.layout_02 = view.findViewById(R.id.community_layout_02);




                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.tv_uname.setText(allDynamics.get(i).get("uname"));
            holder.tv_title.setText(allDynamics.get(i).get("title"));
            holder.tv_content.setText(allDynamics.get(i).get("content"));

            // 格式化时间
            String c_time = allDynamics.get(i).get("c_time");
            long longtime = Long.valueOf(c_time);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(longtime);
            String dateFormat = sdf.format(date);
            holder.tv_time.setText(dateFormat);

            // 2. 设置图片
            // 2.1 通过动态id从 iamgesMap 中获取属于该动态的图片
            allDynamics.get(i);
            allDynamics.get(i).get("did");


            ArrayList<Bitmap> tempImages = imagesBitmapMap.get(allDynamics.get(i).get("did"));
            if (tempImages != null) {
                for (Bitmap bitmapImage:tempImages
                ) {

                    mySetImageView(bitmapImage, holder);
                }
            } else {
            }


            // 同时在这里添加 list_item 的监听事件
            holder.layout_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(CommunityFragment.this.getActivity(), "点击了" + allDynamics.get(i).get("title") + "的title", Toast.LENGTH_SHORT).show();
                    notifyDataSetChanged();
                }
            });

            holder.layout_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(CommunityFragment.this.getActivity(), "点击了" + allDynamics.get(i).get("content") + "的content", Toast.LENGTH_SHORT).show();
                    notifyDataSetChanged();
                }
            });

            holder.layout_star.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(CommunityFragment.this.getActivity(), "点击了" + allDynamics.get(i).get("content") + "的content", Toast.LENGTH_SHORT).show();
                    notifyDataSetChanged();
                }
            });

            // 把 item 对应的布局转换为 View 对象之后，最后记得返回

            return view;
        }

        // 生成一个 ImageView 并设置到布局中
        public void mySetImageView(Bitmap bitmap, ViewHolder holder) {
            // 创建一个临时的 ImageView，并添加到 LinearLayout
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 75, getResources().getDisplayMetrics());

            tempImageView = new ImageView(getContext());

            tempImageView.setTag(bitmap);


            tempImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            LinearLayout.LayoutParams layoutParams =  new LinearLayout.LayoutParams(width, height);
            int margin1 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
            int margin2 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
            int margin3 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
            layoutParams.setMargins(margin1, margin2, margin3, 0);


            tempImageView.setLayoutParams(layoutParams);

            // 设置图片内容
            tempImageView.setImageBitmap(bitmap);

            // 图片数量加1
            imagesCount++;


            if (imagesCount <= 4) {
                holder.layout_01.addView(tempImageView);
            } else if (imagesCount > 4 && imagesCount <= 8) {
                holder.layout_02.addView(tempImageView);
            }

        }


        // 特别注意，在 list_item 里面的控件要在这里声明，然后再实例化
        class ViewHolder {
            TextView tv_uname;
            TextView tv_title;
            TextView tv_content;
            TextView tv_time;
            LinearLayout layout_share, layout_comment, layout_star;

            // 用来存放图片的 LinearLayout
            LinearLayout layout_01, layout_02;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.community_float_btn: {
                Intent intent = new Intent(this.getActivity(), ShareActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.community_bar_follow: {
                Toast.makeText(getContext(), "关注功能暂未开放...", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.community_bar_video: {
                Toast.makeText(getContext(), "视频功能暂未开放...", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.community_bar_search: {
                Toast.makeText(getContext(), "搜索功能暂未开放...", Toast.LENGTH_SHORT).show();

                break;
            }

            // list_item 里面控件的监听事件不能写在这里，要写在 CustomBaseAdapter 的 getView 方法里面
//            case R.id.dynamic_layout_share:
//                break;
//            case R.id.dynamic_layout_comment:
//                break;
//            case R.id.dynamic_layout_star:
//                break;
        }

    }




}