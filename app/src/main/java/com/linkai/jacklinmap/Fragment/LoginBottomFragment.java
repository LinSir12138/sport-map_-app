package com.linkai.jacklinmap.Fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.linkai.jacklinmap.R;

/**
 * 创建日期：2021/6/12 21:29
 * @author 林凯
 * 文件名称： LoginBottomFragment.java
 * 类说明： 登录注册界面底部的 Fragment，用来展示图标
 */
public class LoginBottomFragment extends Fragment implements View.OnClickListener {

    ImageView iv_wechat, iv_qq, iv_weibo;

    public LoginBottomFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_bottom, container, false);
    }


    // 在 Fragment 与 Activity 通信时，必须要等到所有的Fragment都已经初始化完成之后，才能够开始进行，所以要写在 onActivityCreated 里面
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        iv_wechat = this.getActivity().findViewById(R.id.login_fragment_icon_wechat);
        iv_qq = this.getActivity().findViewById(R.id.login_fragment_icon_qq);
        iv_weibo = this.getActivity().findViewById(R.id.login_fragment_icon_weibo);

        iv_wechat.setOnClickListener(this);
        iv_qq.setOnClickListener(this);
        iv_weibo.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_fragment_icon_wechat:
                Toast.makeText(getContext(), "微信登录待开发...", Toast.LENGTH_SHORT).show();
                break;
            case R.id.login_fragment_icon_qq:
                Toast.makeText(getContext(), "QQ登录待开发...", Toast.LENGTH_SHORT).show();
                break;
            case R.id.login_fragment_icon_weibo:
                Toast.makeText(getContext(), "微博登录待开发...", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}