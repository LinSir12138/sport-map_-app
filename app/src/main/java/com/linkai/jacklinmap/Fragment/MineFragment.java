package com.linkai.jacklinmap.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.linkai.jacklinmap.Activity.LoginActivity;
import com.linkai.jacklinmap.Activity.Mine.DynamicHistoryActivity;
import com.linkai.jacklinmap.Activity.Mine.MyFavorateActivity;
import com.linkai.jacklinmap.Activity.Mine.MyFollowActivity;
import com.linkai.jacklinmap.Activity.Mine.MyShareActivity;
import com.linkai.jacklinmap.ObjectDetection.DetectorActivity;
import com.linkai.jacklinmap.R;
import com.linkai.jacklinmap.Activity.Mine.SettingActivity;

/**
 * 创建日期：2021/6/10 10:54
 *
 * @author 林凯
 * 文件名称： MineFragment.java
 * 类说明： “我的” 对应的 Fragment，首页底部第3个按钮对应的 Fragment
 */
public class MineFragment extends Fragment implements View.OnClickListener {


    TextView tv_uname, tv_logout;
    RelativeLayout layout_sport, layout_star, layout_share, layout_follow, layout_setting;

    // 对话框
    AlertDialog alert = null;
    AlertDialog.Builder builder;

    public MineFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mine, container, false);
    }


    // 在 Fragment 与 Activity 通信时，必须要等到所有的Fragment都已经初始化完成之后，才能够开始进行，所以要写在 onActivityCreated 里面
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        // 实例化控件，添加监听事件
        init();


        // 获取数据，显示用户名
        SharedPreferences mSharedPreferences = getActivity().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        final String uname = mSharedPreferences.getString("uname", "");
        final String u_id = mSharedPreferences.getString("u_id", "");
        System.out.println("mine u_id = " + u_id);
        tv_uname.setText(uname);
    }

    private void init() {
        // 实例化 TextView 和 RelativeLayout
        tv_uname = this.getActivity().findViewById(R.id.mine_tv_uname);
        layout_sport = this.getActivity().findViewById(R.id.mine_layout_sport);
        layout_star = this.getActivity().findViewById(R.id.mine_layout_star);
        layout_share = this.getActivity().findViewById(R.id.mine_layout_share);
        layout_follow = this.getActivity().findViewById(R.id.mine_layout_follow);
        layout_setting = this.getActivity().findViewById(R.id.mine_layout_setting);
        tv_logout = this.getActivity().findViewById(R.id.mine_tv_logout);

        // 添加监听事件
        layout_sport.setOnClickListener(this::onClick);
        layout_star.setOnClickListener(this::onClick);
        layout_share.setOnClickListener(this::onClick);
        layout_follow.setOnClickListener(this::onClick);
        layout_setting.setOnClickListener(this::onClick);
        tv_logout.setOnClickListener(this::onClick);

    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.mine_layout_sport:

                intent = new Intent(getContext(), DynamicHistoryActivity.class);
                this.getActivity().startActivity(intent);
                break;
            case R.id.mine_layout_star:

                intent = new Intent(getContext(), MyFavorateActivity.class);
                this.getActivity().startActivity(intent);

                break;
            case R.id.mine_layout_share:

                intent = new Intent(getContext(), DetectorActivity.class);
                this.getActivity().startActivity(intent);
                break;
            case R.id.mine_layout_follow:

                intent = new Intent(getContext(), MyFollowActivity.class);
                this.getActivity().startActivity(intent);

                break;
            case R.id.mine_layout_setting:
                startActivity(new Intent(this.getActivity(), SettingActivity.class));

                break;
            case R.id.mine_tv_logout:


                builder = new AlertDialog.Builder(getContext());

                alert = builder.setIcon(R.mipmap.icon_warning)
                        .setTitle("系统提示：")
                        .setMessage("确定要退出登录吗？")
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Toast.makeText(getContext(), "注销成功", Toast.LENGTH_SHORT).show();


                                // 移除用户登录数据
                                SharedPreferences sp = getContext().getSharedPreferences("userinfo", getContext().MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.remove("uname");
                                editor.remove("uid");
                                editor.apply();

                                // 跳转到登录界面
                                startActivity(new Intent(MineFragment.this.getActivity(), LoginActivity.class));
                                MineFragment.this.getActivity().finish();


                            }
                        }).create();             //创建AlertDialog对象
                alert.show();                    //显示对话框


                break;
        }
    }
}