package com.linkai.jacklinmap.Adapter;

import android.view.ViewGroup;

import com.linkai.jacklinmap.Fragment.CommunityFragment;
import com.linkai.jacklinmap.Fragment.MineFragment;
import com.linkai.jacklinmap.Fragment.RunFragment;
import com.linkai.jacklinmap.Activity.HomeActivity;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    private final int PAGER_COUNT = 3;
    private CommunityFragment communityFragment = null;
    private MineFragment mineFragment = null;
    private RunFragment runFragment = null;



    public MyFragmentPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        communityFragment = new CommunityFragment();
        mineFragment = new MineFragment();
        runFragment = new RunFragment();
    }


    @Override
    public int getCount() {
        return PAGER_COUNT;
    }

    @Override
    public Object instantiateItem(ViewGroup vg, int position) {
        return super.instantiateItem(vg, position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        System.out.println("position Destory" + position);
        super.destroyItem(container, position, object);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case HomeActivity.PAGE_ONE:
                System.out.println("page1");
                fragment = communityFragment;
                break;
            case HomeActivity.PAGE_TWO:
                System.out.println("page2");
                fragment = runFragment;
                break;
            case HomeActivity.PAGE_THREE:
                System.out.println("page3");
                fragment = mineFragment;
                break;
        }
        return fragment;
    }


}

