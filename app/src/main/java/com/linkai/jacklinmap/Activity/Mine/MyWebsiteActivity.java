package com.linkai.jacklinmap.Activity.Mine;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.linkai.jacklinmap.R;

public class MyWebsiteActivity extends AppCompatActivity {

    WebView webView;
    String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_website);


        Intent intent = getIntent();
        address = intent.getStringExtra("address");

        webView = findViewById(R.id.mine_web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(address);
    }
}