package com.linkai.jacklinmap.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.linkai.jacklinmap.Database.MyHelper;
import com.linkai.jacklinmap.R;

/**
 * 创建日期：2021/6/10 18:46
 * @author 林凯
 * 文件名称： RegisterActivity.java
 * 类说明： 注册界面
 */
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher  {

    Button btn_returnlogin, btn_register;
    EditText et_uname, et_phone, et_password, et_password_second;
    ImageButton register_delete_1, register_delete_2, register_delete_3, register_delete_4;

    String uname, phone, password, password_second;

    // 数据库操作
    SQLiteDatabase db;
    MyHelper dbHelper;
    ContentValues values;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        dbHelper = new MyHelper(RegisterActivity.this);

        init();
    }

    private void init() {
        // 返回，注册按钮
        btn_returnlogin = findViewById(R.id.register_returnlogin);
        btn_register = findViewById(R.id.register_register);

        // 输入框
        et_uname = findViewById(R.id.register_uname);
        et_phone = findViewById(R.id.register_phone);
        et_password = findViewById(R.id.register_password);
        et_password_second = findViewById(R.id.register_password_second);

        // 删除按钮
        register_delete_1 = findViewById(R.id.register_delete_1);
        register_delete_2 = findViewById(R.id.register_delete_2);
        register_delete_3 = findViewById(R.id.register_delete_3);
        register_delete_4 = findViewById(R.id.register_delete_4);
        // 设置为隐藏
        register_delete_1.setVisibility(View.GONE);
        register_delete_2.setVisibility(View.GONE);
        register_delete_3.setVisibility(View.GONE);
        register_delete_4.setVisibility(View.GONE);

        // 返回，注册按钮添加监听事件
        btn_returnlogin.setOnClickListener(this::onClick);
        btn_register.setOnClickListener(this::onClick);

        // 输入框添加监听事件
        et_uname.addTextChangedListener(this);
        et_phone.addTextChangedListener(this);
        et_password.addTextChangedListener(this);
        et_password_second.addTextChangedListener(this);

        // 删除按钮添加监听事件
        register_delete_1.setOnClickListener(this::onClick);
        register_delete_2.setOnClickListener(this::onClick);
        register_delete_3.setOnClickListener(this::onClick);
        register_delete_4.setOnClickListener(this::onClick);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_returnlogin:
                finish();
                break;
            case R.id.register_register:

                uname = et_uname.getText().toString();
                phone = et_phone.getText().toString();
                password = et_password.getText().toString();
                password_second = et_password_second.getText().toString();

                if (password_second.equals(password) == false) {
                    Toast.makeText(getApplicationContext(), "两次输入密码不一致", Toast.LENGTH_SHORT).show();;
                    break;
                }

                // 进行数据库操作
                db = dbHelper.getWritableDatabase();
                values = new ContentValues();



                // 这里要和数据库中字段对应
                values.put("u_name", uname);
                values.put("u_phone", phone);
                values.put("u_password", password);

                db.insert("userinfo", null, values);
                db.close();
                Toast.makeText(getApplicationContext(), "注册成功,请登录！", Toast.LENGTH_SHORT).show();;


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //do something
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                    }
                }, 1000);    //延时1s执行

                break;
            case R.id.register_delete_1:
                et_uname.setText("");
                break;
            case R.id.register_delete_2:
                et_phone.setText("");
                break;
            case R.id.register_delete_3:
                et_password.setText("");
                break;
            case R.id.register_delete_4:
                et_password_second.setText("");
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    // 监听 EditText 文本发生改变，显示删除按钮
    @Override
    public void afterTextChanged(Editable s) {

        if (et_uname.getText().length() != 0) {
            register_delete_1.setVisibility(View.VISIBLE);
        } else {
            register_delete_1.setVisibility(View.GONE);
        }
        if (et_phone.getText().length() != 0) {
            register_delete_2.setVisibility(View.VISIBLE);
        } else {
            register_delete_2.setVisibility(View.GONE);
        }
        if (et_password.getText().length() != 0) {
            register_delete_3.setVisibility(View.VISIBLE);
        } else {
            register_delete_3.setVisibility(View.GONE);
        }
        if (et_password_second.getText().length() != 0) {
            register_delete_4.setVisibility(View.VISIBLE);
        } else {
            register_delete_4.setVisibility(View.GONE);
        }
    }
}