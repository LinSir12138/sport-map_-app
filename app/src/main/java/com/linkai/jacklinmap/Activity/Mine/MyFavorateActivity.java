package com.linkai.jacklinmap.Activity.Mine;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.linkai.jacklinmap.R;

/**
 * 创建日期：2021/6/19 15:50
 * @author 林凯
 * 文件名称： MyFavorateActivity.java
 * 类说明：
 *      我的收藏界面（暂未开放）
 */
public class MyFavorateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favorate);
    }
}