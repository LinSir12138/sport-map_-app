package com.linkai.jacklinmap.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.linkai.jacklinmap.MyWidget.CountDownProgressView;
import com.linkai.jacklinmap.R;

/**
 * 创建日期：2021/6/9 15:46
 * @author 林凯
 * 文件名称： SplashActivity.java
 * 类说明： 闪屏界面
 */
public class SplashActivity extends AppCompatActivity {

    CountDownProgressView countDownProgressView;
    SharedPreferences mSharedPreferences;
    String uname;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // 设置有颜色的状态栏
        setStatusBarColor(this, Color.parseColor("#1292d0"));

        init();

        // 获取数据
        mSharedPreferences = this.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        // 如果取不到值，就赋默认值，即空字符串 ""
        uname = mSharedPreferences.getString("uname", "");


        // 点击精度条时
        countDownProgressView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (uname.equals("") == false) {
                    // uname 不是默认值，表名已经登录了
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }
            }
        });

        // 进度条结束时
        countDownProgressView.setProgressListener(new CountDownProgressView.OnProgressListener() {
            @Override
            public void onProgress(int progress) {
                if (progress == 0) {
                    if (uname.equals("") == false) {
                        // 如果已经登录了，直接跳转到首页
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    }
                }
            }
        });
    }

    private void init() {
        countDownProgressView = findViewById(R.id.countDownProgressView);
        countDownProgressView.setTimeMillis(2000);
        countDownProgressView.setProgressType(CountDownProgressView.ProgressType.COUNT_BACK);
        countDownProgressView.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    static void setStatusBarColor(Activity activity, int statusColor) {
        Window window = activity.getWindow();
        //取消状态栏透明
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //添加Flag把状态栏设为可绘制模式
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        //设置状态栏颜色
        window.setStatusBarColor(statusColor);
        //设置系统状态栏处于可见状态
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        //让view不根据系统窗口来调整自己的布局
        ViewGroup mContentView = (ViewGroup) window.findViewById(Window.ID_ANDROID_CONTENT);
        View mChildView = mContentView.getChildAt(0);
        if (mChildView != null) {
            ViewCompat.setFitsSystemWindows(mChildView, false);
            ViewCompat.requestApplyInsets(mChildView);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // 销毁当前Activity
        finish();
    }
}