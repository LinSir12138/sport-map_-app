package com.linkai.jacklinmap.Activity.Mine;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.linkai.jacklinmap.Activity.Mine.MyWebsiteActivity;
import com.linkai.jacklinmap.R;

/**
 * 创建日期：2021/6/18 16:17
 * @author 林凯
 * 文件名称： DeveloperActivity.java
 * 类说明： 用来展示开发者信息，同时使用 WebView 显示网址（网络功能）
 *      (1)在这里测试一下广播功能,实际广播功能再 RunActivity 里面用到了
 */
public class DeveloperActivity extends AppCompatActivity implements View.OnClickListener {

    TextView developer_link_github,developer_link_gitee, developer_link_myblog, developer_link_onlinetool;

    private IntentFilter intentFilter;
    private NetworkChangeReceiver networkChangeReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);

        developer_link_github = findViewById(R.id.developer_link_github);
        developer_link_gitee = findViewById(R.id.developer_link_gitee);
        developer_link_myblog = findViewById(R.id.developer_link_myblog);
        developer_link_onlinetool = findViewById(R.id.developer_link_onlinetool);

        developer_link_github.setOnClickListener(this::onClick);
        developer_link_gitee.setOnClickListener(this::onClick);
        developer_link_myblog.setOnClickListener(this::onClick);
        developer_link_onlinetool.setOnClickListener(this::onClick);


        // 创建 IntentFilter 并添加 action，表明监听什么类型的广播。这里选择监听系统的网络状态
//        intentFilter = new IntentFilter();
//        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
//        networkChangeReceiver = new NetworkChangeReceiver();
//        // 注册动态广播监听器
//        registerReceiver(networkChangeReceiver, intentFilter);
    }


    // 内部类，继承 BroadcastReceiver
    class NetworkChangeReceiver extends BroadcastReceiver {

        // 每当网络状态发生变化，onReceive 方法得到执行
        @Override
        public void onReceive(Context context, Intent intent) {
            // 获得一个系统服务类
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            // 通过这个服务类获取网络信息
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable()) {
                Toast.makeText(context, "网络可用", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "网络不可用", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 动态注册的广播接收器记得取消注册
        if (networkChangeReceiver != null)  {
            unregisterReceiver(networkChangeReceiver);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.developer_link_github:  {

                Intent intent = new Intent(this, MyWebsiteActivity.class);
                intent.putExtra("address", "https://github.com/LinSir12138");
                startActivity(intent);

                break;
            }
            case R.id.developer_link_gitee:  {
                Intent intent = new Intent(this, MyWebsiteActivity.class);
                intent.putExtra("address", "https://gitee.com/LinSir12138");
                startActivity(intent);

                break;
            }
            case R.id.developer_link_myblog:  {
                Intent intent = new Intent(this, MyWebsiteActivity.class);
                intent.putExtra("address", "http://linkaiblog.top");
                startActivity(intent);

                break;
            }
            case R.id.developer_link_onlinetool:  {
                Intent intent = new Intent(this, MyWebsiteActivity.class);
                intent.putExtra("address", "http://linkaiblog.top:8080/onlinetool/#/welcome");
                startActivity(intent);

                break;
            }


        }

    }

}