package com.linkai.jacklinmap.Activity.Community;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.linkai.jacklinmap.Database.MyHelper;
import com.linkai.jacklinmap.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * 创建日期：2021/6/13 21:47
 *
 * @author 林凯
 * 文件名称： DynamicDetailActivity.java
 * 类说明： 动态详情页面，用来展示用户发布的动态的详细信息
 */
public class DynamicDetailActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView iv_back, iv_share;

    ListView listView;

    // 存放的动态信息
    HashMap<String, String> dynamicMap;


    // 存放评论信息
    // 整个 ListView 的评论信息存在 dataList 中，其中的每一个 item 就是 mapItem
    ArrayList<HashMap<String, String>> dataList;
    // 评论数据的每一项
    HashMap<String, String> commentItem;

    // 根据动态id，从数据库中获取的该条动态的图片，所有图片存在一个 ArrayList 里面，每一个 item 就是一张图片
    ArrayList<Bitmap> bitmapArrayList;

    // 数据库操作
    SQLiteDatabase db;
    MyHelper dbHelper;
    ContentValues values;

    ImageView tempImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_detail);

        dbHelper = new MyHelper(DynamicDetailActivity.this);

        // 初始化数据
        initData();

        // 初始化控件
        initWidget();
    }

    private void initData() {

        // 1. 从 CommunityFragment 中获取传递过来的动态详情数据，是一个 Map
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        // 注意，传递过来的时候是套了2个 list，所以这里要去两次
        ArrayList<Parcelable> bundleList = bundle.getParcelableArrayList("data");

        ArrayList tempList = (ArrayList) bundleList.get(0);
        dynamicMap = (HashMap<String, String>) tempList.get(0);

        // 2. 查询评论信息并展示
        queryComments();


        // 3. 获取这条动态说包含的图片，存入 bitmapArrayList 中
        queryImages();

        // 3.为 ListView 设置 Adapter，创建自定义Adapter，继承BaseAdapter 因为 baseAdapter 是一个抽象类
        CustomBaseAdapter customBaseAdapter = new CustomBaseAdapter();
        listView = findViewById(R.id.dynamic_detail_listview);
        listView.setAdapter(customBaseAdapter);
        // 设置 ListView 的 item 项的点击事件
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            }
        });
    }

    private void queryImages() {
        db = dbHelper.getReadableDatabase();
        bitmapArrayList = new ArrayList<>();

        Cursor cursor = db.rawQuery("select * from images where d_id = ?", new String[]{dynamicMap.get("did")});

        if (cursor.getCount() == 0) {
            Toast.makeText(getApplicationContext(), "暂无图片...", Toast.LENGTH_SHORT).show();
        } else {
            cursor.moveToFirst();
            do {
                // 将 sqilte 里面的 blob 转换成为 bitmap
                Bitmap bitmap = BitmapFactory.decodeByteArray(cursor.getBlob(3), 0, cursor.getBlob(3).length);
                bitmapArrayList.add(bitmap);
            } while (cursor.moveToNext());

        }

    }

    // 更新评论数据 即更新 dataList, 则 ListView 里面的数据会自动更新
    private void queryComments() {
        db = dbHelper.getReadableDatabase();
        dataList = new ArrayList<HashMap<String, String>>();

        // 查询这条动态下面对应的所有的评论信息
        // 从 CommunityFragment 传递过来的 dataMap（存放动态信息） 中获取动态的id --> did
        Cursor cursor = db.rawQuery("select * from comment where d_id = ? order by c_id desc", new String[]{dynamicMap.get("did")});
        if (cursor.getCount() == 0) {
            Toast.makeText(getApplicationContext(), "暂无评论...", Toast.LENGTH_SHORT).show();
        } else {
            cursor.moveToFirst();
            do {
                commentItem = new HashMap<>();

                //  0            1                 2               3                         4         5
                //  c_id        u_id             d_id             f_id                  content     c_time
                //  评论id   发表评论的用户id  评论对应的动态id    评论的父id，没有则为-1    评论内容    评论时间
                commentItem.put("c_id", cursor.getString(0));
                commentItem.put("u_id", cursor.getString(1));
                commentItem.put("d_id", cursor.getString(2));
                commentItem.put("f_id", cursor.getString(3));
                commentItem.put("content", cursor.getString(4));
                commentItem.put("c_time", cursor.getString(5));


                // 根据评论id查询评论用户的用户名
                Cursor cursorUname = db.rawQuery("select u_name from userinfo where u_id = ?", new String[]{cursor.getString(1)});
                //存在数据才返回true
                if (cursorUname.moveToFirst()) {
                    commentItem.put("uname", cursorUname.getString(0));
                }
                cursorUname.close();

                dataList.add(commentItem);
            } while (cursor.moveToNext());

        }

    }

    private void initWidget() {

        // 初始化控件
        iv_back = findViewById(R.id.dynamic_detail_back);
        iv_share = findViewById(R.id.dynamic_detail_share);


        // 添加监听事件
        iv_back.setOnClickListener(this::onClick);
        iv_share.setOnClickListener(this::onClick);


    }

    // 定义一个内部类，继承自 BaseAdapter，并重写其方法
    private class CustomBaseAdapter extends BaseAdapter {

        final int TYPE_1 = 0;
        final int TYPE_2 = 1;

        // 因为 listView 的第一项不是评论，所以这里要稍微修改一下
        @Override
        public int getCount() {
            System.out.println("datalistsize");
            System.out.println(dataList.size() + 1);
            return dataList.size() + 1;
        }

        @Override
        public Object getItem(int i) {
            if (i == 0) {
                return null;
            } else {
                return dataList.get(i - 1);
            }
        }

        @Override
        public int getItemViewType(int position) {
            // 为不同的 item 设置不同的布局
            if (position == 0) {
                return TYPE_1;
            } else {
                return TYPE_2;
            }
        }

        @Override
        public int getViewTypeCount() {
            // 共有2中布局
            return 2;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        // 这个方法事件我们 ListView 里面为 item 定义的样式文件转换成为一个 View 对象
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            int type = getItemViewType(i);
            System.out.println("type = " + type);

            switch (type) {
                case TYPE_1: {
                    // ListView 的第一个 item，用来展示动态的具体内容

                    ViewHolderFirst holderFirst;
                    if (view == null) {
                        // 设置 item 的布局文件
                        view = View.inflate(DynamicDetailActivity.this, R.layout.listview_item_dynamic_detail_info, null);
                        holderFirst = new ViewHolderFirst();

                        // 实例化 list_item 中的 TextView 控件  注意这里一定是 view.findViewById() 不能直接 findViewById()
                        holderFirst.tv_uname = (TextView) view.findViewById(R.id.dynamic_detail_uname);      // 动态发布者的用户名
                        holderFirst.tv_title = (TextView) view.findViewById(R.id.dynamic_detail_tv_title);      // 动态标题
                        holderFirst.tv_time = (TextView) view.findViewById(R.id.dynamic_detail_time);      // 动态标题
                        holderFirst.tv_content = (TextView) view.findViewById(R.id.dynamic_detail_info_tv_content);      // 动态内容
                        holderFirst.linearLayout01 = view.findViewById(R.id.dynamic_detail_linearlayout_01);    // 放置图片的 linearlayout
                        holderFirst.linearLayout02 = view.findViewById(R.id.dynamic_detail_linearlayout_02);    // 放置图片的 linearlayout

                        // item 中的 button 控件，EditView 控件
                        holderFirst.et_content = (EditText) view.findViewById(R.id.dynamic_detail_et_content);      // 评论文本内容
                        holderFirst.btn_clear = (Button) view.findViewById(R.id.dynamic_detail_btn_clean);        // 清空评论按钮
                        holderFirst.btn_comment = (Button) view.findViewById(R.id.dynamic_detail_btn_comment);    // 发布评论按钮

                        view.setTag(holderFirst);
                    } else {
                        holderFirst = (ViewHolderFirst) view.getTag();
                    }

                    // 为 txtView 赋值（展示动态具体内容）,从 CommunityFragment 中获取传递过来的动态详情信息
                    holderFirst.tv_uname.setText(dynamicMap.get("uname").toString());     // 动态发布者用户名
                    holderFirst.tv_title.setText(dynamicMap.get("title"));     // 动态标题
                    holderFirst.tv_content.setText(dynamicMap.get("content"));     // 动态内容

                    // 展示图片
                    System.out.println("size = " + bitmapArrayList.size());
                    System.out.println(bitmapArrayList);
                    for (int j = 0; j < bitmapArrayList.size(); j++) {
                        mySetImageView(bitmapArrayList.get(j), holderFirst, j);
                    }

                    // 格式化事件
                    String c_time = dynamicMap.get("c_time");
                    long longtime = Long.valueOf(c_time);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date(longtime);
                    String dateFormat = sdf.format(date);

                    holderFirst.tv_time.setText(dateFormat);        // 动态发布时间


                    // 为清空动态按钮，发布动态按钮添加监听事件
                    holderFirst.btn_clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holderFirst.et_content.setText("");
                        }
                    });

                    // 发布评论内容
                    holderFirst.btn_comment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // 进行数据库操作
                            db = dbHelper.getWritableDatabase();
                            values = new ContentValues();

                            // 1. 获取数据存入 values 中,这里要和数据库中字段对应
                            // 1.3 从文件中获当前用户的id
                            SharedPreferences mSharedPreferences = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                            values.put("u_id", mSharedPreferences.getString("u_id", null));

                            // 1.4 从 intent 中获取动态的 id
                            values.put("d_id", dynamicMap.get("did"));
                            // 1.5 设置父评论id，由于这里是直接评论动态，没有父评论，设置为 -1
                            values.put("f_id", "-1");
                            values.put("content", holderFirst.et_content.getText().toString());
                            values.put("c_time", new Date().getTime());

                            db.insert("comment", null, values);
                            db.close();

                            Toast.makeText(getApplicationContext(), "发布成功！", Toast.LENGTH_SHORT).show();
                            holderFirst.et_content.setText("");

                            // 更新评论信息并展示
                            queryComments();
                        }
                    });

                    break;
                }
                case TYPE_2: {
                    // 后续的 item，用来展示 评论
                    // 优化的 BaseAdapter，使用 ViewHolder
                    ViewHolderSecond holderSecond;
                    if (view == null) {
                        // 设置 item 的布局文件
                        view = View.inflate(DynamicDetailActivity.this, R.layout.listview_item_dynamic_detail_comment, null);
                        holderSecond = new ViewHolderSecond();

                        // 实例化 list_item 中的 TextView 控件
                        holderSecond.tv_uname = (TextView) view.findViewById(R.id.list_item_comment_uname);
                        holderSecond.tv_time = (TextView) view.findViewById(R.id.list_item_comment_time);
                        holderSecond.tv_content = (TextView) view.findViewById(R.id.list_item_comment_content);

                        view.setTag(holderSecond);

                    } else {
                        holderSecond = (ViewHolderSecond) view.getTag();
                    }

                    // 特别注意：这里因为第1项不是评论，所以要写成 i - 1
                    // 这里detalist.get(i) 获得的是 MapItem，存放的是评论信息
                    holderSecond.tv_uname.setText(dataList.get(i - 1).get("uname"));
                    holderSecond.tv_content.setText(dataList.get(i - 1).get("content"));

                    // 格式化事件
                    String c_time_2 = dataList.get(i - 1).get("c_time");
                    long longtime_2 = Long.valueOf(c_time_2);
                    SimpleDateFormat sdf_2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date_2 = new Date(longtime_2);
                    String dateFormat_2 = sdf_2.format(date_2);

                    holderSecond.tv_time.setText(dateFormat_2);

                    // 同时在这里添加 list_item 的监听事件
                    holderSecond.tv_content.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(getApplicationContext(), "点击了" + "content", Toast.LENGTH_SHORT).show();
                            notifyDataSetChanged();
                        }
                    });
                    break;
                }
            }

            // 把 item 对应的布局转换为 View 对象之后，最后记得返回
            return view;
        }

        // 生成一个 ImageView 并设置到布局中
        public void mySetImageView(Bitmap bitmap, ViewHolderFirst holder, int index) {
            // 创建一个临时的 ImageView，并添加到 LinearLayout
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 75, getResources().getDisplayMetrics());


            tempImageView = new ImageView(DynamicDetailActivity.this);

            tempImageView.setTag(bitmap);


            tempImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
            int margin1 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
            int margin2 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
            int margin3 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
            layoutParams.setMargins(margin1, margin2, margin3, 0);


            tempImageView.setLayoutParams(layoutParams);

            // 设置图片内容
            tempImageView.setImageBitmap(bitmap);

            if (index <= 4) {
                holder.linearLayout01.addView(tempImageView);
            } else {
                holder.linearLayout02.addView(tempImageView);
            }
        }

        // 特别注意，在 list_item 里面的控件要在这里声明，然后再实例化

        // ListView 的一个 item 的控件的资源，用来展示动态详情
        class ViewHolderFirst {
            // 动态详情：发布动态的用户名，动态标题，动态内容
            TextView tv_uname, tv_title, tv_time, tv_content;
            // 评论文本内容
            EditText et_content;
            // 清空评论按钮，发布评论按钮
            Button btn_clear, btn_comment;

            LinearLayout linearLayout01, linearLayout02;

        }

        // listView 的后续的 item 的控件资源，用来展示评论
        class ViewHolderSecond {
            TextView tv_uname;
            TextView tv_time;
            TextView tv_content;        // 评论内容
        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dynamic_detail_back:
                // 返回按钮：销毁当前活动
                finish();
                break;
            case R.id.dynamic_detail_share:
                // 分享按钮：
                Toast.makeText(getApplicationContext(), "暂未开放...", Toast.LENGTH_SHORT).show();
                break;
        }

    }
}