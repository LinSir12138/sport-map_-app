package com.linkai.jacklinmap.Utils;

import java.util.ArrayList;

/**
 * 创建日期：2021/6/22 15:32
 *
 * @author 林凯
 * 文件名称： CategoryCOCO.java
 * 类说明： coco 数据集对应的 80 个分类
 */
public class MyCategory {

    ArrayList<String> category;



    public MyCategory() {
        category = new ArrayList<String>(){{

            add("AE86");
            add("finger skateboard");
            add("folding fan");
            add("gyroscope");
        }};

    }

    public ArrayList<String> getCategory() {
        return category;
    }
}
