package com.linkai.jacklinmap.Utils;

import java.util.ArrayList;

/**
 * 创建日期：2021/6/22 15:32
 *
 * @author 林凯
 * 文件名称： CategoryCOCO.java
 * 类说明： coco 数据集对应的 80 个分类
 */
public class COCOCategory {

    ArrayList<String> category;



    public COCOCategory() {
        category = new ArrayList<String>(){{

            add("person");
            add("bicycle");
            add("car");
            add("motorbike");
            add("aeroplane");
            add("bus");
            add("train");
            add("truck");
            add("boat");
            add("traffic light");
            add("fire hydrant");
            add("stop sign");
            add("parking meter");
            add("bench");
            add("bird");
            add("cat");
            add("dog");
            add("horse");
            add("sheep");
            add("cow");
            add("elephant");
            add("bear");
            add("zebra");
            add("giraffe");
            add("backpack");
            add("umbrella");
            add("handbag");
            add("tie");
            add("suitcase");
            add("frisbee");
            add("skis");
            add("snowboard");
            add("sports ball");
            add("kite");
            add("baseball bat");
            add("baseball glove");
            add("skateboard");
            add("surfboard");
            add("tennis racket");
            add("bottle");
            add("wine glass");
            add("cup");
            add("fork");
            add("knife");
            add("spoon");
            add("bowl");
            add("banana");
            add("apple");
            add("sandwich");
            add("orange");
            add("broccoli");
            add("carrot");
            add("hot dog");
            add("pizza");
            add("donut");
            add("cake");
            add("chair");
            add("sofa");
            add("potted plant");
            add("bed");
            add("dining table");
            add("toilet");
            add("tvmonitor");
            add("laptop");
            add("mouse");
            add("remote");
            add("keyboard");
            add("cell phone");
            add("microwave");
            add("oven");
            add("toaster");
            add("sink");
            add("refrigerator");
            add("book");
            add("clock");
            add("vase");
            add("scissors");
            add("teddy bear");
            add("hair drier");
            add("toothbrush");
        }};

    }

    public ArrayList<String> getCategory() {
        return category;
    }
}
